### Yplayer: youtube audio player desktop application

### Releases
- [v1.0.0-beta](https://bitbucket.org/haikel2090/yplayer-versions/downloads/yplayer_Setup_1.0.0.exe)

### Author
Haikel Fazzani

### License
MIT